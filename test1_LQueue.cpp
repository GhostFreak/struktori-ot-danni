#include <iostream>
#include"Queue.h"
using namespace std;



class CNode {
public:
    CNode()
    {
    	m_pNext = NULL;
    	m_pPrev = NULL;
	}
    CNode(int in_data)
    {
    	m_data = in_data;
    	m_pNext = NULL;
    	m_pPrev = NULL;
	}
    ~CNode();

    int  m_data;
    CNode* m_pNext;
    CNode* m_pPrev;
};
template <typename F>
class LQueue:public CQueue<F> 
{
public:
	F* m_pHead;
	int Cap;
	int MCap;
	F* Top;
	int T_counter;
	F* Bottom;
	int B_counter;
	
	LQueue(int cap,int mcap) 
	{
        Top= NULL;
        Bottom=NULL;
        Cap=cap;
        MCap=mcap;
        T_counter=0;
        B_counter=0;
    }

    virtual~LQueue() {};
    virtual void enqueue(F* pInEl) throw(QueueOverflowException)
    {
    	if(T_counter<Cap)
    	{
    		if(T_counter!=0)
    		{
				Top->m_pNext=pInEl;
				pInEl->m_pPrev=Top;
				Top=pInEl;
				T_counter++;
			}
			else
			{
				Top=pInEl;
				Bottom=pInEl;
				T_counter++;
				B_counter++;
			}
		}
		else
		{
			int newCap=Cap*2;
			if(newCap>MCap)
			{
				throw QueueOverflowException();
			}
			else
			{
				Cap=newCap;
				Top->m_pNext=pInEl;
				pInEl->m_pPrev=Top;
				Top=pInEl;
				T_counter++;
			}
		}
    	
	}
    virtual F* dequeue() throw(QueueEmptyException)
    {
    	if(Bottom!=NULL)
    	{
    		F* ret_el=NULL;
    		ret_el=Bottom;
    		Bottom=Bottom->m_pNext;
    		
    		return ret_el;
		}
		else
		{
			throw QueueEmptyException();
		}
	}
   

};

int main()
{
	LQueue<CNode>q(3,6);
	CNode* out=NULL;
	CNode* a=new CNode (1);
	CNode* b=new CNode (2);
	CNode* c=new CNode (3);
	CNode* d=new CNode (4);
	CNode* e=new CNode (5);
	CNode* f=new CNode (6);
	CNode* g=new CNode (7);
	
	
	q.enqueue(a);
	q.enqueue(b);
	q.enqueue(c);
	q.enqueue(d);
	q.enqueue(e);
	q.enqueue(f);
	//q.enqueue(g);
	
	
	
	out=q.dequeue();
	cout<<out->m_data;
	out=q.dequeue();
	cout<<out->m_data;
	out=q.dequeue();
	cout<<out->m_data;
	out=q.dequeue();
	cout<<out->m_data;
	out=q.dequeue();
	cout<<out->m_data;
	out=q.dequeue();
	cout<<out->m_data;
	//out=q.dequeue();
	//cout<<out->m_data;
	
	
	return 0;
}







