#ifndef EXCEPTION_H
#define EXCEPTION_H

#include <string>
using namespace std;

class RuntimeException {
    public:
        RuntimeException(const string& err) { errorMsg = err; }
        string getMessage() const { return errorMsg; }

    private:
        string errorMsg;

};

#endif // EXCEPTION_H
