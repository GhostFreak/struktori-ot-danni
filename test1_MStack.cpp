
#include <iostream>
#include "CStack.h"
using namespace std;


template <typename T>
class MStack :public CStack<T>
{
public:
	T**m_pStackTop;
    int m_nTop;
    int m_nCap;
    int m_nMCap;
    
	MStack(){};
    MStack(int cap, int max)
    {
        m_nCap=cap;
        m_nMCap=max;
        m_pStackTop=new T* [m_nCap];
		m_nTop=0;

    }
    ~MStack(){};
   virtual void push(T* pInEl) throw(StackOverflowException)
    {
    	if(m_nTop<m_nCap)
    	{
        	m_pStackTop[m_nTop]=pInEl;
        	m_nTop++;
    	}
    	else
        {
        	int new_Cap=m_nCap*2;
        	if(new_Cap>m_nMCap)
        	{
        		throw(StackOverflowException());
			}
            else
            {
            	T** New_StackTop=new T*[m_nCap];
            	for(int i=0;i<m_nCap;i++)
            	{
            		New_StackTop[i]=m_pStackTop[i];
				}
				
				int old_Cap=m_nCap;
				m_nCap=new_Cap;
				delete m_pStackTop;
				m_pStackTop=new T*[m_nCap];
				
				for(int i=0;i<old_Cap;i++)
				{
					m_pStackTop[i]=New_StackTop[i];
				}
				
				m_pStackTop[m_nTop]=pInEl;
				m_nTop++;
			}
        }
	}
    virtual T* pop() throw(StackEmptyException)
    {
    	  if(m_nTop==0)
          {
              throw (StackEmptyException());
          }
          	T* pElement=NULL;
          	m_nTop--;
		  	pElement=m_pStackTop[m_nTop];

		  	return pElement;



	}

};



int main()
{
	MStack<int>s(3,6);
	int* a=new int(1);
	int* b=new int(2);
	int* c=new int(3);
	int* d=new int(4);
	int* e=new int(5);
	int* f=new int(6);
	s.push(a);
	s.push(b);
	s.push(c);
	s.push(d);
	s.push(e);
	s.push(f);
	cout<<*(s.pop());
	cout<<*(s.pop());
	cout<<*(s.pop());
	cout<<*(s.pop());
	cout<<*(s.pop());
	cout<<*(s.pop());


	return 0;
}









