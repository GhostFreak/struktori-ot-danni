#include <iostream>
#include "Queue.h"

using namespace std;

template <typename T>
class MQueue: public CQueue<T> {
public:
    MQueue(int cap,int maxcap)
    {
        m_qTop = 0;
        m_qBottom = 0;
        m_nCap = cap;
        m_MCap=maxcap;
        m_pElements = new T*[m_nCap];
        m_pElements[0]=NULL;
    }
    virtual ~MQueue()
    {
        delete[] m_pElements;
    }

    virtual void enqueue(T* pInEl) throw(QueueOverflowException)
    {
            if (m_qTop < m_nCap ) {
                m_pElements[m_qTop] = pInEl;
                m_qTop++;
            }
            else 
			{
  
				int newcap=m_nCap*2;
				if(newcap>m_MCap)
				{
					if(m_pElements[m_qTop%m_nCap]==NULL)
            		{
            			m_pElements[m_qTop%m_nCap]= pInEl;
            			m_qTop++;	
					}
					else{throw QueueOverflowException();}
				}
				else
				{
					T** New_El=new T*[m_nCap];
					for(int i=0;i<m_nCap;i++)
					{
						New_El[i]=m_pElements[i];
					}
					int old_cap=m_nCap;
					m_nCap=newcap;
					delete m_pElements;
					m_pElements=new T*[m_nCap];
					for(int i=0;i<old_cap;i++)
					{
						m_pElements[i]=New_El[i];
					}
					delete New_El;
					m_pElements[m_qTop]= pInEl;
            		m_qTop++;	
				}
            }
            
    }
    void bot(int &m_qBottom,int m_nCap)
    {
           if(m_qBottom>=m_nCap){ 
		   m_qBottom=m_qBottom%m_nCap;
		   }
	}
    virtual T* dequeue() throw(QueueEmptyException)
    {
           bot(m_qBottom,m_nCap);
		   
		    if (m_pElements[m_qBottom]==NULL ) {
                throw QueueEmptyException();
            }

            else  {
            	T* ret_Element=m_pElements[m_qBottom];
            	m_pElements[m_qBottom]=NULL;
                m_qBottom++;
				return ret_Element;
            }
    }

private:
    int m_qTop;
    int m_qBottom;
    int m_nCap;
    int m_MCap;
    T** m_pElements;
};



int main ()
{
	MQueue <int> q(3,6);
	int* a=new int(1);
	int* b=new int(2);
	int* c=new int(3);
	int* d=new int(4);
	int* e=new int(5);
	int* f=new int(6);

	q.enqueue(a);
	q.enqueue(b);
	q.enqueue(c);
	q.enqueue(d);
	q.enqueue(e);
	q.enqueue(f);

	cout<<*(q.dequeue());
	cout<<*(q.dequeue());
	cout<<*(q.dequeue());
	cout<<*(q.dequeue());
	cout<<*(q.dequeue());
	cout<<*(q.dequeue());


	

	





    return 0;
}
