#include <iostream>
#include "Queue.h"

using namespace std;

template <typename TElement>
class MQueue: public CQueue<TElement> {
public:
    MQueue(int cap)
    {
        m_qTop = 0;
        m_qBottom = 0;
        m_nCap = cap;
        m_pElements = new TElement*[m_nCap];
        m_pElements[0]=NULL;
    }
    virtual ~MQueue()
    {
        delete[] m_pElements;
    }

    virtual void enqueue(TElement* pInEl) throw(QueueOverflowException)
    {
            if (m_qTop < m_nCap ) {
                m_pElements[m_qTop] = pInEl;
                m_qTop++;
            }
            else {
            	if(m_pElements[m_qTop%m_nCap]==NULL)
            	{
            		m_pElements[m_qTop%m_nCap]= pInEl;
            		m_qTop++;	
				}
				else{throw QueueOverflowException();}
            }
            
    }
    void bot(int &m_qBottom,int m_nCap)
    {
           if(m_qBottom>=m_nCap){ 
		   m_qBottom=m_qBottom%m_nCap;
		   }
	}
    virtual TElement* dequeue() throw(QueueEmptyException)
    {
           bot(m_qBottom,m_nCap);
		   
		    if (m_pElements[m_qBottom]==NULL ) {
                throw QueueEmptyException();
            }

            else  {
            	TElement* ret_Element=m_pElements[m_qBottom];
            	m_pElements[m_qBottom]=NULL;
                m_qBottom++;
				return ret_Element;
            }
    }

private:
    int m_qTop;
    int m_qBottom;
    int m_nCap;
    TElement** m_pElements;
};



int main ()
{
	MQueue <int> q(3);
	int* a=new int(1);
	q.enqueue(a);
	int* b=new int(2);
	q.enqueue(b);
	int* c=new int(3);
	q.enqueue(c);
	cout<<*(q.dequeue());
	int* d=new int(4);
	q.enqueue(d);
	cout<<*(q.dequeue());
	cout<<*(q.dequeue());
	cout<<*(q.dequeue());
	

	





    return 0;
}
