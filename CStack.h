#ifndef CITB306_2016_CSTACK_H
#define CITB306_2016_CSTACK_H

#include "Exception.h"

class StackOverflowException : public RuntimeException {
public:
    StackOverflowException() : RuntimeException("Stack overflow") {}
};

class StackEmptyException : public RuntimeException {
public:
    StackEmptyException() : RuntimeException("Stack empty") {}
};

template <typename TStackEl>
class CStack {
public:
        CStack(){};
        virtual ~CStack(){};

        virtual void push(TStackEl* pInElement) throw (StackOverflowException) = 0;
        virtual TStackEl* pop() throw (StackEmptyException) = 0;
};

#endif // CITB306_2016_CSTACK_H
