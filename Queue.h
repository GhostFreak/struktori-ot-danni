#include "Exception.h"

class QueueEmptyException : public RuntimeException {
    public:
        QueueEmptyException() : RuntimeException("Empty Queue!") {}
};

class QueueOverflowException : public RuntimeException {
    public:
        QueueOverflowException() : RuntimeException("Queue overflow!") {}
};



template <typename TElement>
class CQueue {
public:
    CQueue() {};
    virtual ~CQueue() {};

    virtual void enqueue(TElement* pInEl) throw(QueueOverflowException) = 0;
    virtual TElement* dequeue() throw(QueueEmptyException) = 0;
};
