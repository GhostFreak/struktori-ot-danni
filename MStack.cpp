
#include <iostream>
#include "CStack.h"
using namespace std;


template <typename TElement>
class MStack :public CStack<TElement>
{
public:
    MStack(){};
    MStack(int cap)
    {
        m_nCap=cap;
        m_pStackTop=new TElement* [m_nCap];
		m_nTop=0;

    }
    ~MStack(){};
   virtual void push(TElement* pInEl) throw(StackOverflowException)
    {
    	if(m_nTop<m_nCap-1)
    	{
        	m_pStackTop[m_nTop]=pInEl;
        	m_nTop++;
    	}
    	else
        {
            throw(StackOverflowException());
        }
	}
    virtual TElement* pop() throw(StackEmptyException)
    {
    	  if(m_nTop==0)
          {
              throw (StackEmptyException());
          }
          	TElement* pElement=NULL;
		  	pElement=m_pStackTop[m_nTop-1];
		  	m_nTop--;
		  	return pElement;



	}

    TElement**m_pStackTop;
    int m_nTop;
    int m_nCap;

};



int main()
{
	MStack<int>stack_ (4);
	//int get=*pop();
	int num=22;
	int* dot=new int (num);
	stack_.push(dot);
	int*newdot=NULL;
	newdot= stack_.pop();
	cout<< *newdot;

	return 0;
}









