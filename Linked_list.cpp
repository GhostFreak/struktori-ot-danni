#include <iostream>
#include <fstream>
#include <string>
using namespace std;

class CData {
public:
    CData() {};
    CData(int prio, int val, string data) {
        m_nPrio = prio;
        m_nValue = val;
        m_sData = data;
    };

    friend ostream& operator<<(ostream& out, const CData& data) {
        out << data.m_nPrio << " " << data.m_nValue << " " << data.m_sData;
        return out;
    }

    friend istream& operator>>(istream& in, CData& data) {
        in >> data.m_nPrio >> data.m_nValue >> data.m_sData;
        return in;
    }

private:
    int m_nPrio;
    int m_nValue;
    string m_sData;
};

class CNode {
public:
    CNode();
    CNode(CData in_data);
    ~CNode();

    CData  m_data;
    CNode* m_pNext;
};

CNode::CNode()
{
    m_pNext = NULL;
}

CNode::~CNode() {}

CNode::CNode(CData in_data) {
    m_data = in_data;
    m_pNext = NULL;
}


class CLinkedList {
public:
    CLinkedList() {
        m_pHead = NULL;
    }

    ~CLinkedList() {
        free_list();
    }

    CNode* m_pHead;
    void print_to_file(ostream& out);
   	void print();
    void append(CNode* pNewNode);
    void free_list();
    int List_size();
    void delete_node(const int nPos);
	void insert_node(const int nPos, CNode* pNewNode);


};




void CLinkedList::print_to_file(ostream& out)
{
	CNode* pList = m_pHead;
    while (pList!=NULL) {
        out<< pList->m_data << endl;
        pList = pList->m_pNext;
    }
    out<< endl;


}
void CLinkedList::print() {
    CNode* pList = m_pHead;
    while (NULL != pList) {
        cout << pList->m_data << endl;
        pList = pList->m_pNext;
    }
    cout << endl;
}


void CLinkedList::append(CNode* pNewNode) {

    if (NULL == m_pHead) {
        m_pHead = pNewNode;
        m_pHead->m_pNext = NULL;
    }
    else {
        CNode* pList = m_pHead;
        while (NULL != pList->m_pNext) {
            pList = pList->m_pNext;
        }
        pList->m_pNext = pNewNode;
        pList->m_pNext->m_pNext = NULL;
    }
}

void CLinkedList::free_list() {
    CNode* pList = m_pHead;

    while (NULL != pList) {
        CNode* pN = pList->m_pNext;
        delete pList;
        pList = pN;
    }

    m_pHead = NULL;
}

void CLinkedList::insert_node(const int nPos, CNode* pNewNode)
{
    if(nPos>0)
    {
    	int count_=List_size();
    	if(m_pHead!=NULL&&nPos<=count_)
		{
            CNode* pNode=m_pHead;
        	if(nPos==1)
        	{
          		pNewNode->m_pNext=m_pHead;
            	m_pHead=pNewNode;
			}
			else
			{
				for(int i=0;i<nPos;i++)
            	{
            		if(i==nPos-2)
            		{
              			pNewNode->m_pNext=pNode->m_pNext;
						pNode->m_pNext=pNewNode;
					}

             		pNode=pNode->m_pNext;
				}
			}

       	}
       	else
        {
        	append(pNewNode);
        	//IF the List is empty
		}

    }
}
void CLinkedList::delete_node(const int nPos)
{
        if(m_pHead!=NULL && nPos>0)
        {
			int count_=List_size();
	        if(nPos<=count_)
			{
	            CNode* pNode=m_pHead;
	            CNode* flag1=NULL;
	            if(nPos==1)
	            {
	            	m_pHead=pNode->m_pNext;
	            	delete pNode;
				}
				else
				{
					for(int i=0;i<nPos;i++)
	            	{
	            		if(i==nPos-2)
	                	{
	                		flag1=pNode;
						}
						if(i==nPos-1)
						{
							flag1->m_pNext=pNode->m_pNext;
							flag1=NULL;
							delete flag1;
							delete pNode;
						}
	                	pNode=pNode->m_pNext;
					}

				}

	        }

        }

}
int CLinkedList::List_size()
{
	int count_=0;
	CNode* pCount = m_pHead;
	while (pCount!= NULL)
	{
		pCount = pCount->m_pNext;
		count_++;
	}
	return count_;
}





int read_from_file_to_linked_list(char* file_name, CLinkedList& LList) {

    ifstream input;
	input.open(file_name);

	if(input.fail()) return -1;

	CData data;
	CNode* newfile=NULL;
	while(!input.eof())
    {
        input>>data;
        newfile=new CNode(data);
        LList.append(newfile);

    }

	input.close();

}

int wrtie_file_from_linked_list(char* file_name, CLinkedList& LList) {
	ofstream output;
	output.open(file_name);
	if(output.fail()) return -1;


	LList.print_to_file(output);
    output.close();

	return 0;
}

int main(int argc, char** argv) {
    if (argc < 3) {
        cout << "Usage: " << endl;
        cout << argv[0] << " in_file_name out_file_name";
        return -1;
    }

    CLinkedList myList;




    if (0 > read_from_file_to_linked_list(argv[1], myList)) {
        cout << "Can't read from file: " << argv[1];
        return -1;
    }
    if (0 > wrtie_file_from_linked_list(argv[2], myList)) {
        cout << "Can't write to file: " << argv[2];
        return -2;
    }
    myList.print();

    return 0;
}
